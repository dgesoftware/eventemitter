# EventEmitter


Browser / Nodejs eventemitter as AMD module, modeled after nodejs eventemitter
https://nodejs.org/dist/latest-v6.x/docs/api/events.html#events_class_eventemitter

```
npm install git+ssh://git@bitbucket.org/dgesoftware/eventemitter.git
```
Nodejs required configuration:

```
var requirejs = require('requirejs');
// var assert = require('assert');
requirejs.config({
  // Pass the top-level main.js/index.js require
  // function to requirejs so that node modules
  // are loaded relative to the top-level JS file.
  paths :{
    modules:'../node_modules'
  },
  nodeRequire: require
});

```
